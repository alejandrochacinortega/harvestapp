## Install

`brew tap wix/brew`

`brew install applesimutils`

`npm install -g detox-cli`

## Build & Run (release)
`detox build --configuration ios.sim.release`

`detox test --configuration ios.sim.release`

## Build & Run (debug)
`yarn e2e-server`

`detox build --configuration ios.sim.debug`

`detox test --configuration ios.sim.debug`
