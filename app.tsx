import React, { Component } from 'react';
import {
  AsyncStorage,
} from 'react-native';
import { Provider } from 'react-redux';
import { persistStore, persistReducer } from 'redux-persist';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import axios from 'axios';
import RootNavigator from './app/navigators/RootNavigator';
import rootReducer from './app/redux/rootReducer';
import { PersistGate } from 'redux-persist/integration/react';
import { setTopLevelNavigator } from './app/navigators/NavigationService';
import { updateLocationSuccess, updateLocationFailed } from './app/redux/locationReducer';


const composer = typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose // eslint-disable-line

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
}

const store = createStore(
  persistReducer(persistConfig, rootReducer),
  composer(applyMiddleware(logger, thunk))
)

const persistor = persistStore(store)

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('./app/redux/rootReducer', () => {
    const nextRootReducer = require('./app/redux/rootReducer');
    store.replaceReducer(nextRootReducer);
  });
}

axios.interceptors.request.use(request => {
  console.log('Starting Request', request)
  return request
})

axios.interceptors.response.use(response => {
  console.log('Response:', response)
  return response
})



interface Props {}
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <RootNavigator ref={setTopLevelNavigator} />
        </PersistGate>
      </Provider>

    );
  }
}
