import React, { Component } from 'react';
import {
  ScrollView, Text, Button, FlatList, View, ActivityIndicator, StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import colors from '../../utils/colors';
import { fetchHours } from '../../redux/harvestReducer';
import ListItem from '../../components/ListItem';


interface Props {
  fetchHours: (() => void)
  location: {
    position: Object
  },
  harvest: {
    hours: {
      time_entries: Array<{id: number}>,
    }
  },
}

interface State {}

class HoursScreen extends Component<Props, State> {
  static navigationOptions = {
    tabBarLabel: 'Hours',
    tabBarTestID: 'tabbar.hours',    
  }

  constructor(props:any) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    this.props.fetchHours();
  }


  render() {
    const { harvest } = this.props;
    if (!harvest.hours) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }
    return (
      <FlatList
        style={styles.scrollViewContainer}
        data={harvest.hours.time_entries}
        renderItem={({ item }) => (
          <ListItem            
            item={item}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }
}

const mapStateToProps = (state:any) => ({
  location: state.location,
  harvest: state.harvest,
});


export default connect(mapStateToProps, { fetchHours })(HoursScreen);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.yellow,
  },

  scrollViewContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: 20,
  },
});
