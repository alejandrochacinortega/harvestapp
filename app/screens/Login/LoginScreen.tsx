import React, { PureComponent } from 'react';
import { connect } from 'react-redux'
import {
  StyleSheet, WebView, NavState, Text, View
} from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { login } from '../../redux/authReducer';
import { HARVEST_CLIENT_ID } from '../../config';
import colors from '../../utils/colors';

interface Props {
  login: typeof login,
  navigation: NavigationScreenProp<{}>,
  accessToken: string,
}

interface State {
}

class LoginScreen extends React.PureComponent<Props, State> {
  componentDidMount() {
    if (this.props.accessToken) {
      setTimeout(() => this.props.navigation.navigate({routeName: 'Tab'}), 1500)
    }
  }

  onNavigationStateChange = (navState:NavState) => {
    if (navState.url && navState.url.indexOf('/rndemoapp/loggedIn') > 0) {
      console.warn(navState.url)
      const [baseUrl, query] = navState.url.split('?')
      const args: {[index:string]: string} = {}
      query.split('&').forEach(part => {
        const [key, value] = part.split('=')
        args[key] = value
      })
      this.props.login(args['access_token'])
      this.props.navigation.navigate({routeName: 'Tab'})

    }
  }

  render() {
    if (this.props.accessToken) {
      return <View style={styles.container}>
        <Text style={styles.loggingIn}>Logging you in...</Text>
      </View>
    }

    return (<WebView
      onNavigationStateChange={this.onNavigationStateChange}
      style={styles.container}
      source={{uri: `https://id.getharvest.com/oauth2/authorize?client_id=${HARVEST_CLIENT_ID}&response_type=token`}}
      />)
  }
}

const mapStateToProps = (state:any) => ({
  location: state.location,
  auth: state.auth,
});
export default connect(mapStateToProps, { login })(LoginScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.beige,
  },
  loggingIn: {
    alignSelf: 'center',
  }
})