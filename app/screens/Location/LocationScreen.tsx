import React, { PureComponent } from 'react';
import {
  View, Text, Button, StyleSheet, Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProp } from 'react-navigation';
import { IndicatorViewPager } from 'rn-viewpager'
import { updateLocationFailed, updateLocationSuccess } from '../../redux/locationReducer';
import MapView, { Region, Circle, Marker } from 'react-native-maps';
import colors from '../../utils/colors';

const dimensions = Dimensions.get('window');

interface Props {
  navigation: NavigationScreenProp<{}>,
  updateLocationSuccess: (position:Position) => void,
  updateLocationFailed: (error:PositionError) => void,
  location: {
    distance: number,
    isUserAtWork: boolean,
    position: {
      coords: {
        latitude: number,
        longitude: number
      }
    },
    workCoords: {
      latitude: number,
      longitude: number
    }
    error: any
  },
}

interface State {}
class LocationScreen extends PureComponent<Props, State> {
  static navigationOptions = {
    tabBarLabel: 'Location',
    tabBarTestID: 'tabbar.location',    
  }

  watchId = 0  

  constructor(props:Props) {
    super(props);
    this.state = {};    
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
  }

  componentDidMount() { 
    if (this.props.location && this.props.location.position) {
      this.fetchLocation()
    }   
  }

  fetchLocation = () => {
    navigator.geolocation.watchPosition((position:any) => {  
      this.props.updateLocationSuccess(position)
    },
    (error) => {
      console.warn(error)
      this.props.updateLocationFailed(error)      
    },
    {
      enableHighAccuracy: true,
      maximumAge: 1000,
      timeout: 20000,
    })
  }

  getCurrentRegion(): Region | undefined {
    if (!this.props.location.position)
      return

    return {
      latitude: this.props.location.position.coords.latitude,
      longitude: this.props.location.position.coords.longitude,
      latitudeDelta: 0.0022,
      longitudeDelta: 0.0022,
    }
  }

  getDistance = () => {
    const { location } = this.props;
    const meters = location.distance > 1 ? 'mts' : 'mt' 
    const distance = `${Math.round(location.distance)} ${meters}`
    return distance
  }

  renderLocationInfo(location:any) {
    return (
      <View style={styles.locationInfoContainer}>
        <View style={styles.infoBox}>
          <Text style={styles.infoText}>
            Distance between locations
          </Text>
          <Text style={styles.grayText}>{this.getDistance()} </Text>
        </View>
        <View style={[styles.statusBox, 
          !location.isUserAtWork && styles.yellowBackground]}>        
          <Text
          testID={"userStatus"}
          style={[styles.infoText, styles.grayText]}>
          {location.isUserAtWork ?
             'User is online :)' : 'User is not at work :('}  
          </Text>
        </View>

      </View>
    );
  }

  render() {
    const { location } = this.props;

    if (location && !location.error && !location.position) {
      return (
        <View style={styles.container}>
        <Button
          onPress={this.fetchLocation}
          title={"Grant Permission"}
          testID="btn.location"
        >          
        </Button>          
        </View>
      )
    }
    
    if (location && location.error && !location.position) {      
      return (
        <View style={styles.container}>
          <Text testID={"error"}>Please allow location permission in order to use the app</Text>
        </View>
      )
    }

    const region = this.getCurrentRegion()
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={region}
        >
        {region && <Circle
          center={{
            latitude: region.latitude,
            longitude: region.longitude,
          }}
          strokeColor="rgba(0,0,0,0)"
          fillColor="rgba(100,128,255,0.2)"
          radius={50}
        />}
        <Marker
          coordinate={location.workCoords}
          title={'Work Location'}
          description={'Here is where I work :)'}
        />        
        </MapView>

        {this.renderLocationInfo(location)}
      </View>
    );
  }
}

const mapStateToProps = (state:any) => ({
  location: state.location,
});


export default connect(mapStateToProps,
  { updateLocationSuccess, updateLocationFailed })(LocationScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.beige,
  },
  map: {
    flex: 1,
    alignSelf: 'stretch',
  },
  locationInfoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  infoBox: {
    flexDirection: 'row',    
    width: '95%',
    height: 50,
    justifyContent: 'space-between',
    alignItems: 'center',    
    backgroundColor: '#FED9D9'    ,
    paddingHorizontal: 20,
    borderRadius: 10,        
  },
  statusBox: {
    backgroundColor: colors.green,    
    shadowColor: 'rgba(97,30,235,0.3)',
    shadowOffset: { width: 5, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,    
    justifyContent: 'center',
    alignItems: 'center',       
    width: '60%',
    height: 50,    
    paddingHorizontal: 20,
    borderRadius: 10,   
    marginVertical: 10, 
  },
  infoText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#454D52'
  },
  locationList: {
    alignSelf: 'stretch',
    height: 44,    
  },
  locationContainer: {
    padding: 10,
    width: dimensions.width,
  },
  locationName: {
    backgroundColor: '#C9C9D9',
    color: 'black',
    height: 44,
  },
  grayText: {
    color: colors.gray
  },
  whiteText: {
    color: colors.white
  },
  orangeBackground: {
    backgroundColor: colors.orange
  },
  yellowBackground: {
    backgroundColor: colors.yellow
  }

});
