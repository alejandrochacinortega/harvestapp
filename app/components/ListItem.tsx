import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../utils/colors';

interface Props {
  item: any
}

const ListItem = (props:Props) => {
  const { item } = props;  
  return (
    <View testID={`time-entry-${item.id}`} style={[
      styles.container,
      { backgroundColor: item.is_running ? colors.green : colors.orange },
    ]}
    >
    <View>
      <Text style={styles.title}>Project: {item.project.name}</Text>
        <Text style={styles.hours}>Hours: {item.hours}</Text>
      </View>
      <View style={styles.iconContainer}>        
        {item.is_running ?
        <Icon 
          testID="running" 
          name={"unlock"} 
          size={25} 
          color={colors.yellow} 
        /> :
        <Icon 
          testID="not-running"
          name={"lock"}
          size={20}
          color={colors.yellow} 
        />        
        }      
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    paddingHorizontal: 8,  
    marginVertical: 2,
    backgroundColor: '#F9E1E1',   
    justifyContent: 'space-between',
    paddingVertical: 10,  
    flexDirection: 'row',   
  },
  title: {
    fontSize: 16,
    color: colors.gray,
    fontWeight: 'bold',
  },
  hours: {
    fontSize: 14,
    color: colors.gray
  },  
  iconContainer: {
    justifyContent: 'center'
  },
});

export default ListItem;
