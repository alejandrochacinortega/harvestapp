import React from 'react';
import { NavigationActions, NavigationContainerComponent, NavigationAction } from 'react-navigation'

let navigatorRef: NavigationContainerComponent;
export function setTopLevelNavigator(navRef:NavigationContainerComponent) {
  navigatorRef = navRef;
}

export const getTopLevelNavigator = () => navigatorRef

export const dispatchNav = (action:NavigationAction) => navigatorRef.dispatch(action)
