import axios from 'axios';
import harvestService from '../utils/harvestService';

export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS';
export const AUTH_LOGIN_FAILED = 'AUTH_LOGIN_FAILED';

export const getAccessToken = (state:{auth:AuthState}) => state.auth.accessToken

export interface LoginAction {
  type: typeof AUTH_LOGIN | typeof AUTH_LOGIN_SUCCESS | typeof AUTH_LOGIN_FAILED
  accessToken: string,
  error: any
}

export function loginSuccess(accessToken:string) {
  return {
    type: AUTH_LOGIN_SUCCESS,
    accessToken
  }
}

export function loginFailed(error:any) {
  return {
    type: AUTH_LOGIN_FAILED,
    error
  }
}

export function login(accessToken:string) {
  return ((dispatch:any, getState:any) => {
    harvestService.get('/v2/users/me', accessToken)
    .then(data => dispatch(loginSuccess(accessToken)))
    .catch(error => console.warn(error) || dispatch(loginFailed(error)))
  })
}

export type AuthAction =
  LoginAction
interface AuthState {
  accessToken: string
}

export default function (state:AuthState = {
  accessToken: '',
}, action:AuthAction) {
  switch (action.type) {
    case AUTH_LOGIN:
      return {
        ...state,
      };
    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        accessToken: action.accessToken,
        error: null
      };

    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        accessToken: '',
        error: action.error
      };
    default:
      return state;
  }
}
