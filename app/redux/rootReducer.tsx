import { combineReducers } from 'redux';
import location from './locationReducer';
import auth from './authReducer';
import harvest from './harvestReducer';

const rootReducer = combineReducers({
  location,
  auth,
  harvest,
});

export default rootReducer;
