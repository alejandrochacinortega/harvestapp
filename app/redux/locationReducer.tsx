import axios from 'axios';
import { getDistanceBetweenTwoLocations } from '../utils/utils';
import harvestService from '../utils/harvestService';
import { fetchHours, setCurrentTask, cleanCurrentTask } from './harvestReducer';

export const UPDATE_LOCATION = 'UPDATE_LOCATION';
export const ADD_LOCATION = 'ADD_LOCATION';
export const REMOVE_LOCATION = 'REMOVE_LOCATION';
export const ENTER_LOCATION = 'ENTER_LOCATION';
export const LEAVE_LOCATION = 'LEAVE_LOCATION';
export const UPDATE_DISTANCE = 'UPDATE_DISTANCE';
export const UPDATE_LOCATION_SUCCESS = 'UPDATE_LOCATION_SUCCESS';
export const UPDATE_LOCATION_FAILED = 'UPDATE_LOCATION_FAILED';
export const SET_WORK_LOCATION = 'SET_WORK_LOCATION';

export interface AddLocation {
  type: typeof ADD_LOCATION,
  name: string,
  position: {
    latitude: number,
    longitude: number,
  }
}

export interface RemoveLocation {
  type: typeof REMOVE_LOCATION,
  index: number,
}

export interface EnterLocation {
  type: typeof ENTER_LOCATION,
  location: Location,
}

export interface LeaveLocation {
  type: typeof LEAVE_LOCATION,
  location: Location,
}

export interface UpdateLocation {
  type: typeof UPDATE_LOCATION_SUCCESS | typeof UPDATE_LOCATION_FAILED,
  position: Object
  error: any
}

export interface UpdateDistance {
  type: typeof UPDATE_DISTANCE,
  distanceBetweenTwoLocations: number
}

export interface SetWorkLocation {
  type: typeof SET_WORK_LOCATION,
  latitude: number,
  longitude: number
}

export function updateLocationSuccess(position:any) {
  return ((dispatch: Function, getState: Function) => {
    const { latitude, longitude } = position.coords;
    const { auth, location } = getState();
    const { accessToken } = auth

    // first time permissions
    if (!location.workCoords) {
      dispatch(setWorkLocation(position.coords))
    }

    const distanceBetweenTwoLocations = getDistanceBetweenTwoLocations(
      latitude,
      longitude,
      location.workCoords ? location.workCoords.latitude : latitude,
      location.workCoords ? location.workCoords.longitude : longitude,
      'K',
    );

    dispatch({ type: UPDATE_LOCATION_SUCCESS, position });
    dispatch({ type: UPDATE_DISTANCE, distanceBetweenTwoLocations });
    if (distanceBetweenTwoLocations < 0.05) {
      const params = {
        "project_id": 11205650,
	      "task_id": 4994192,
        "spent_date": new Date().toISOString()
      }
      harvestService.post(
        '/v2/time_entries',
        params,
        accessToken
      )
      .then(response => {
        dispatch(setCurrentTask(response.data))
        dispatch(fetchHours())
      })
      .catch((error) => {
        console.log('Error ', error.message);
      });
    } else {
        const { harvest } = getState()
        if (harvest.currentTask) {
          const timeEnryId = harvest.currentTask.id
          harvestService.patch(
            `/v2/time_entries/${timeEnryId}/stop`,
            null,
            accessToken
          )
          .then((task) => {
            dispatch(cleanCurrentTask())
            dispatch(fetchHours())
          })
          .catch(error => console.log('Error stopping task ', error))
        } else {
          dispatch(fetchHours())
        }
    }
  });

}

export function updateLocationFailed(error:any) {
  return {
    type: UPDATE_LOCATION_FAILED,
    error,
  };
}

export function setWorkLocation (coords:any) {
  return {
    type: SET_WORK_LOCATION,
    latitude: coords.latitude,
    longitude: coords.longitude
  }
}

export interface Location {
  name: string,
  position: {
    latitude: number,
    longitude: number,
  }
}
export interface LocationState {
  locations: Location[],
  currentLocation?: Location
}

export type LocationAction =
  AddLocation |
  RemoveLocation |
  EnterLocation |
  LeaveLocation |
  UpdateLocation |
  UpdateDistance |
  SetWorkLocation

export default function (state:LocationState = {
  locations: [],
}, action: LocationAction) {
  switch (action.type) {
    case ADD_LOCATION:
      return {
        ...state,
        locations: [...state.locations, {
          name: action.name,
          position: action.position
        }],
        error: null,
      }
    case REMOVE_LOCATION:
      return {
        ...state,
        locations: state.locations.splice(action.index, 1),
        error: null,
      }
    case UPDATE_LOCATION_SUCCESS:
      return {
        ...state,
        position: action.position,
        error: null,
      };
    case UPDATE_LOCATION_FAILED:
      return {
        ...state,
        coords: null,
        error: action.error,
      };
    case UPDATE_DISTANCE:
      return {
        ...state,
        distance: action.distanceBetweenTwoLocations,
        isUserAtWork: action.distanceBetweenTwoLocations < 0.05,
      };
    case SET_WORK_LOCATION:
      return {
        ...state,
        workCoords: {
          latitude: action.latitude,
          longitude: action.longitude
        }
      };
    default:
      return state;
  }
}
